import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import {Container, Navbar, Nav} from 'react-bootstrap';
import { AppNavbar } from './components/AppNavbar.js';
import Register from './pages/Register.js'

function App() {
  return (
    <div className="App">
      <Router>
        <AppNavbar />
        <Routes>
          <Route path = "/" />
          <Route path="/register" element={<Register/>}/>
          <Route path = "/cart" />
        </Routes>
      </Router>
    </div>
  );
}

export default App;