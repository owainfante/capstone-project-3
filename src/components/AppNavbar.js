import React from "react";
import {Container, Navbar, Nav} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom'
import { ShoppingCart } from "phosphor-react";

export const AppNavbar = () => {
    return (
        <Navbar>
            <Container>
                <Nav className = "mr-auto">
                    <Nav.Link as={NavLink} to="/">
                        Home
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="./pages/register">
                        Register
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/cart">
                        <ShoppingCart size={32}/>
                    </Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    );
};